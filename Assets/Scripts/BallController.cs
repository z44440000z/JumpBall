﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [Header("Physics Setting")]
    public float height = 2;
    public float speed = 1;
    private Rigidbody rb;
    private bool isforce;

    [Header("Restart Setting")]
    private Vector3 startPos;
    public GameObject RelifePanel;
    public GameObject ClearPanel;

    [Header("Super Speed Setting")]
    public int perfectPass = 0;//穿過的空隙
    private bool ignoreNextCollision;
    public bool isSuperSpeedActive;//超級加速

    [Header("Particle Setting")]
    public GameObject particle;
    RaycastHit hit;
    public float offest=0.1f;
    private SphereCast sphereCast;
    // [Header("Explosion Setting")]
    // public float radius = 2F;
    // public float power = 5F;
    private void Awake()
    {
        startPos = transform.position;
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        sphereCast = GetComponent<SphereCast>();
    }

    // Update is called once per frame
    void Update(){}
    void FixedUpdate()
    {
        if (isforce)
        { force(); }
        if (perfectPass >= 3 && !isSuperSpeedActive)
        { isSuperSpeedActive = true; }
        else
        { rb.AddForce(Physics.gravity.y * Vector3.up * speed, ForceMode.Acceleration); }
    }

    void force()
    {
        isforce = false;
        //Vector3 newV = Vector3.up * Mathf.Sqrt(height);
        //rb.velocity = newV * Mathf.Abs(speed);
        rb.AddForce(Vector3.up * height, ForceMode.Impulse);
    }

    public void ResetBall()
    {
        GameManager.Instance.isDead=false;
        RelifePanel.SetActive(false);
        ClearPanel.SetActive(false);
        rb.isKinematic = false;
        transform.position = startPos;
        foreach(Marking marking in FindObjectsOfType<Marking>()){
            Destroy(marking.gameObject);
        }
        FindObjectOfType<CameraController>().Resetcam();
    }

    // void CheckCollision()
    // {
    //     if (Physics.Raycast(transform.position - new Vector3(0, 1f, 0), Vector3.down, out hit,
    //     FindObjectOfType<HelixController>().leveldistance / 3))
    //     {
    //         GameObject p = Instantiate(particle, hit.point + new Vector3(0, 0.01f, 0), Quaternion.Euler(90, Random.Range(0, 360), 0));
    //         p.transform.parent = hit.transform;
    //     }
    // }
    void CreateMarking(Transform parent){
        GameObject p = Instantiate(particle, sphereCast.markingVec + new Vector3(0,offest,0), Quaternion.Euler(90, Random.Range(0, 360), 0));
        p.transform.parent = parent;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (ignoreNextCollision)
        { return; }
        if (other.transform.tag == "Goal")
        {
            ClearPanel.SetActive(true);
            rb.isKinematic = true;
            CreateMarking(other.transform);
        }
        if (isSuperSpeedActive)
        {
            if (!other.transform.GetComponent<Goal>())
            { 
                // Destroy(other.transform.parent.gameObject); 
                foreach(Transform trans in other.transform.parent.GetComponentsInChildren<Transform>()){
                    if(trans.gameObject != other.transform.parent.gameObject)
                    { 
                        Destroy(trans.gameObject);
                        // Rigidbody transRb = trans.GetComponent<Rigidbody>();
                        // transRb.isKinematic=false;
                        // transRb.useGravity=false;
                        // transRb.AddExplosionForce(power, Vector3.up, radius, 3.0F); 
                    }
                }
            }
        }
        else
        {
            if (other.transform.tag == "Death")
            {
                RelifePanel.SetActive(true);
                rb.isKinematic = true;
                Death deathPart = other.transform.GetComponent<Death>();
                deathPart.HittedDeathPart();
                GameManager.Instance.isDead=true;
                CreateMarking(other.transform);
            }
            if (other.transform.tag == "Ground")
            {
                isforce = true;
                rb.velocity = Vector3.zero; // Remove velocity to not make the ball jump higher after falling done a greater distance
                CreateMarking(other.transform);
            }

            if (isSuperSpeedActive)
            {
                if (other.transform.tag != "Goal")
                {
                    Destroy(other.transform.parent.gameObject);
                }
            }
        }
        // Safety check
        ignoreNextCollision = true;
        Invoke("AllowCollision", .2f);

        // Handlig super speed
        perfectPass = 0;
        isSuperSpeedActive = false;
        force();
    }

    private void AllowCollision()
    {
        ignoreNextCollision = false;
    }
}
