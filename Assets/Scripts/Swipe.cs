﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{
    private float startRotationY = 0;
    private float startTouchX = 0;
    public Vector3 StartTouchPos;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                startRotationY = transform.rotation.y;
                StartTouchPos = Input.touches[0].position;
                startTouchX = Input.touches[0].position.x;
            }
            if (Input.touches[0].phase == TouchPhase.Moved)
            {
                float delta = Input.touches[0].position.x - startTouchX;
                float deltaX = Input.touches[0].position.x - StartTouchPos.x;
                float deltaY = Input.touches[0].position.y - StartTouchPos.y;
                if (Mathf.Abs(deltaX) < Mathf.Abs(deltaY))
                { }
                else
                {
                    transform.rotation = Quaternion.Euler(0, startRotationY - delta, 0);
                }
            }
        }
    }
}
