﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    public BallController player;
    private void Awake()
    {
        player = FindObjectOfType<BallController>();
    }

    void Update()
    {
        if (player.transform.position.y < transform.position.y)
            FollowPlayer();
    }
    private void FollowPlayer()
    {
        transform.position = new Vector3(transform.position.x, player.transform.position.y, transform.position.z);
    }
    public void Resetcam()
    {
        FollowPlayer();
    }
}
