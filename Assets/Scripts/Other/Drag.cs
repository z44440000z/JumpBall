﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drag : MonoBehaviour
{
    public GameObject Cylinder;
    private float startScaleY = 0;
    private float startTouchY = 0;

    // Start is called before the first frame update
    void Start()
    {
        Cylinder = GetComponent<Transform>().gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                startScaleY = transform.localScale.y;
                startTouchY = Input.touches[0].position.y;
            }
            if (Input.touches[0].phase == TouchPhase.Moved)
            {
                float delta = Input.touches[0].position.y - startTouchY;
                Cylinder.transform.localScale = new Vector3(transform.localScale.x, startScaleY + delta / 100, transform.localScale.z);
                if (Cylinder.transform.localScale.y <= 0)
                { Cylinder.transform.localScale = new Vector3(Cylinder.transform.localScale.x, delta / 100, -Cylinder.transform.localScale.z); }
                Debug.Log(delta);
            }
        }
    }
}
