﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereCast : MonoBehaviour
{
    public GameObject currentHitObject;

    public float sphereRadius;
    public float maxDistance;
    public LayerMask layerMask;

    private Vector3 origin;
    private Vector3 direction;
    private float currentHitDistance;
    public Vector3 markingVec;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        origin = transform.position;
        direction = -Vector3.up;
        RaycastHit hit;
        if(Physics.SphereCast(origin,sphereRadius,direction,out hit,maxDistance,layerMask,QueryTriggerInteraction.UseGlobal)){
            currentHitObject = hit.transform.gameObject;
            currentHitDistance=hit.distance;
            markingVec= hit.point;
            // GameObject p = Instantiate(particle, hit.point, Quaternion.Euler(90, Random.Range(0, 360), 0));
        }
        else{
            currentHitDistance=maxDistance;
            currentHitObject=null;
        }
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Debug.DrawLine(origin,origin + direction*currentHitDistance);
        Gizmos.DrawWireSphere(origin+ direction*currentHitDistance,sphereRadius);
    }
}
